var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
	email: { type: String, lowercase: true, trim: true, required: true, index: { unique: true } },
	password: { type: String, required: true, select: false },
	firstName: {type: String, required: true},
	lastName: {type: String, required: true},
});

module.exports = mongoose.model('User', UserSchema);
