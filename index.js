var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var mongoose = require('mongoose')
var connection = mongoose.connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo')
var UserModel = require('./userModel')

app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.send('Just a test')
})

app.get('/users', function (req, res) {
	UserModel.find(function (err, results) {
		res.send(results);
	})
})

app.post('/users', function (req, res) {

	var user = new UserModel();
	
	user.email = req.body.email;
	user.firstName = req.body.firstName;
	user.lastName = req.body.lastName;
	user.password = req.body.password;

	user.save(function(err, newUser) {
		if (err)
			res.send(err);
		else
			res.send(newUser);
	});

})

app.listen(8080, function () {
  console.log('Example app listening on port 8080!')
})